const geografis = require("geografis");

const dump = () => geografis.dump();

const search = query => geografis.search(query, parseInt(process.env.SEARCH_MAX), 0);

const province = () => geografis.getProvinces();

const province_code = code => geografis.getProvince(code);

const province_slug = slug => geografis.getProvinceBySlug(slug);

//////////////////////////////////////////////////////////////////

const city_code = code => geografis.getCity(code);

const city_slug = slug => geografis.getCityBySlug(slug);

//////////////////////////////////////////////////////////////////

const district_code = code => geografis.getDistrict(code);

const district_slug = slug => geografis.getDistrictBySlug(slug);

//////////////////////////////////////////////////////////////////

const village_code = code => geografis.getVillage(code);

const village_slug = slug => geografis.getVillageBySlug(slug);

// const search = query => geografis.search(query, 10, 0);

module.exports = {
	dump,
	search,
	province,
	province_code,
	province_slug,
	
	city_code,
	city_slug,

	district_code,
	district_slug,
}