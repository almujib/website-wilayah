require("dotenv").config();

const express = require("express");
const app = express();

const data = require("./data");

app.get("/", (req, res) => {
	const context = {
		pages: {
			"all": [
				"Menampilkan seluruh data",
				"PERINGATAN: APABILA BROWSER ERROR UNTUK MENAMPUNG SELURUHNYA!"
				],
			"search?q=": [
				"Mencari kode wilayah, kode pos, nama desa/kelurahan, kecamatan, dan kota menggunakan elasticlunr.js (sedikit berat).",
				`Secara default maksimal hasil yang ditampilkan adalah ${process.env.SEARCH_MAX}`
				],
			"province": "Menampilkan seluruh provinsi.",
			"province-code?code=": "Menampilkan provinsi berdasarkan kode",
			"province-slug?slug=": "Mendapatkan detil provinsi berdasarkan slug (parameter slug).",
			"city-code": "Mendapatkan detil kota/kabupaten berdasarkan kode wilayah (parameter code).",
			"city-slug": "Mendapatkan detil provinsi berdasarkan slug (parameter slug).",
			"district": "Mendapatkan detil kecamatan berdasarkan slug",
		}
	}

	res.json(context);
});

app.get("/all", (req, res) => {
	res.json(data.dump());
});

app.get("/search", (req, res) => {
	const context = data.search(req.query.q);
	res.json(context);
});

app.get("/province", (req, res) => {
	res.json(data.province());
});

app.get("/province-code", (req, res) => {
	const context = data.province_code(req.query.code);
	res.json(context);
});

app.get("/province-slug", (req, res) => {
	const context = data.province_slug(req.query.slug);
	res.json(context);
});

/////////////////////////////////////////////////////////

app.get("/city-code", (req, res) => {
	const context = data.city_code(req.query.code);
	res.json(context);
});

app.get("/city-slug", (req, res) => {
	const context = data.city_slug(req.query.slug);
	res.json(context);
});

app.get("/district-code", (req, res) => {
	const context = data.district_code(req.query.code);
	res.json(context);
});


app.listen(3000, () => console.log("Listening on port 3000"));